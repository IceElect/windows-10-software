<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('programs')) {
            Schema::create('programs', function (Blueprint $table) {
                $table->id();
                $table->char('slug', 255);
                $table->char('name', 255);
                $table->text('desc')->nullable();
                $table->text('meta_desc')->nullable();
                $table->char('thumbnail', 255)->nullable();
                $table->char('company', 128)->nullable();
                $table->char('site', 255)->nullable();
                $table->char('category', 255)->nullable();
                $table->char('file_size', 255)->nullable();
                $table->char('url', 255)->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programs');
    }
}
