<?php

use App\ProgramCategory;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgramCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_categories', function (Blueprint $table) {
            $table->id();
            $table->char('name', 255);
            $table->timestamps();
        });

        ProgramCategory::create(['name' => 'Registry Cleaners']);

        Schema::table('programs', function (Blueprint $table) {
            // $table->dropColumn(['category']);
            $table->foreignId('category_id')->after('name')->default('1')->constrained('program_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('program_categories');
    }
}
