var $ = require( "jquery" );

$('.program__slider').slick({
    arrows: true,
    adaptiveHeight: true,
    autoplay: true,
    autoplaySpeed: 5000,
    prevArrow: '<div class="program__slider-prev"></div>',
    nextArrow: '<div class="program__slider-next"></div>',
});

$('.program__description-tabs-link').on('click', function(e) {
    e.preventDefault();

    var currentTab = $(this).data('tab');

    $('.program__description-tabs-link_active').removeClass('program__description-tabs-link_active');
    $('.program__description-tabs-text_active').removeClass('program__description-tabs-text_active');

    $('.program__description-tabs-link[data-tab="' + currentTab + '"]').addClass('program__description-tabs-link_active');
    $('.program__description-tabs-text[data-tab="' + currentTab + '"]').addClass('program__description-tabs-text_active');

    return false;
})
