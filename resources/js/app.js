// require('./bootstrap');
var $ = require( "jquery" );
require('readmore-js');
require('../plugins/slick/slick.min.js');
require('./program');


/* READMORE START */
var rows = 4;
var lineHeight = $('.content-readmore').css('line-height');
lineHeight = lineHeight.substring(0, lineHeight.length - 2);
$('.content-readmore').readmore({
    speed: 200,
    collapsedHeight: rows * lineHeight + 13,
    moreLink: '<a href="#" class="more">Read more...</a>',
    lessLink: '<a href="#" class="more">Close</a>'
});
/* READMORE END */
