@extends('layouts.app')

@section('content')
<section>
    <div class="container">
        <div class="section-title">
            <h1 class="section-title__inner">@yield('title')</h1>
        </div>
        <div class="section-content">
            @yield('text')
        </div>
    </div>
</section>
@endsection
