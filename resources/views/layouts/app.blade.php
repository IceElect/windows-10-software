<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('pageTitle') | {{ env('APP_NAME') }}</title>

    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/css/app.css">

    @hasSection('head')
        @yield('head')
    @endif
</head>
<body>

    <header class="header">
        <div class="container">
            <div class="header__inner row">
                <div class="header__left col-md-4">
                    <a href="{{ Route('home') }}" class="header__logo">
                        <img src="/images/logo.png" alt="{{ env('APP_NAME') }}">
                    </a>
                </div>
                <div class="header__right col-md-8">
                    <form action="{{ Route('home') }}" class="header__search-form">
                        <input type="text" name="q" class="header__search-form-field field" @if( !empty($q) )value="{{ $q }}" @endif placeholder="Type to search...">
                        <button type="button" type="submit" class="header__search-form-btn"><i class="fas fa-search"></i></button>
                    </form>
                    <div class="header__social">
                        <a href="#" class="header__social-link"><i class="fab fa-facebook-f"></i></a>
                        <a href="#" class="header__social-link"><i class="fab fa-twitter"></i></a>
                        <a href="#" class="header__social-link"><i class="fab fa-google-plus-g"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    @yield('content')

    <footer class="footer">
        <div class="container">
            <div class="footer__inner row">
                <div class="footer__copy col-md-6">
                    © 2020 Zone10.Software. All rights reserved.
                </div>
                <div class="footer__menu col-md-6">
                    <a href="{{ Route('eula') }}" class="footer__menu-link">EULA</a>
                    <a href="{{ Route('terms') }}" class="footer__menu-link">Terms of Use</a>
                    <a href="{{ Route('privacy') }}" class="footer__menu-link">Privacy Policy</a>
                    <a href="{{ Route('contacts') }}" class="footer__menu-link">Contact us</a>
                </div>
                <p class="col-12 footer__text">
                    Zone10.Software is a review-site of free third-party software. All trademarks, registered trademarks, product names and company names or logos mentioned herein are the property of their respective owners.
                </p>
            </div>
        </div>
    </footer>

    <script src="/js/app.js"></script>

    @hasSection('scripts')
        @yield('scripts')
    @endif
</body>
</html>
