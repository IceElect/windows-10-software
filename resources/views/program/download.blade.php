@extends('layouts.app')

@section('pageTitle', 'Download ' . $program->name . ' for Windows 10 (64 bit/32 bit)')

@section('head')
    <meta name="description" content="Need fresh software for Windows 10 64 or 32 bit? Download Latest versions on Zone10.Software!">
    <meta name="keywords" content="software for windows 10,programs for windows 10,download for windows 10,Windows 10 Software">
@endsection

@section('content')
    <section class="download">
        <div class="container">
            <h1 class="download__heading">Your {{ $program->name }} download should start in a few seconds...</h1>

            <script>
                setTimeout('window.location.href="{{ $program->installer['path'] }}"', 3000);
            </script>

            <div class="download__related">
                <h2 class="download__related-heading">Users also downloads:</h2>
                <div class="download__related-items">
                    @foreach($random_programs as $item)
                    <a href="{{ Route('program.view', ['slug' => $item->slug]) }}" class="download__related-item">
                        <img src="{{ $item->thumbnail() }}" alt="{{ $item->name }}" class="download__related-item-thumbnail">
                        <span class="download__related-item-name">{{ $item->name }}</span>
                        <span class="download__related-item-category">{{ $item->category->name }}</span>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection
