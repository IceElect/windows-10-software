@extends('layouts.app')

@section('pageTitle', 'Download ' . $program->name . ' for Windows 10. Free Latest ' . $program->name . ' for Windows 10 (64 bit/32 bit)')

@section('head')
    <meta name="description" content="{{ $program->meta_desc }}">
    <meta name="keywords" content="{{ $program->name }} windows 10,{{ $program->name }} for windows 10,{{ $program->name }} windows 10 download,download {{ $program->name }} for windows 10,download {{ $program->name }} for windows 10 64 bit">
@endsection

@section('content')
    <section class="program">
        <div class="container">
            <div class="program__inner row">
                <div class="program__left col-lg-3 col-sm-5">
                    <div class="program__info">
                        <a href="#" class="program__info-thumbnail">
                            <img src="{{ $program->thumbnail() }}" alt="{{ $program->name }}">
                        </a>
                        <div class="program__info-content">
                            <div class="program__info-name">{{ $program->name }}</div>
                            <div class="program__info-company">{{ $program->company }}</div>
                        </div>
                    </div>
                    <dl class="program__details">
                        <dt>Category</dt>
                        <dd>{{ $program->category->name }}</dd>

                        <dt>Last Updated</dt>
                        <dd>{{ date('Y-m-d') }}</dd>

                        <dt>File size</dt>
                        <dd>{{ $program->installer['size'] }}</dd>

                        <dt>Rating</dt>
                        <dd class="program__details-rating">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </dd>

                        <dt>Operating system</dt>
                        <dd>Windows 10 (64/32 bit)</dd>

                        <dt>Compatible with</dt>
                        <dd>Windows 7,8/8.1,XP,Vista</dd>
                    </dl>

                    <a href="{{ Route('program.download', ['slug' => $program->slug]) }}" rel="nofollow" class="program__download">
                        <i class="program__download-icon fas fa-chevron-double-right"></i>
                        <span class="program__download-text">
                            <span class="program__download-title">Download</span>
                            <span class="program__download-counter">{{ $program->downloads_count }} downloads</span>
                        </span>
                    </a>

                    <small class="program__mention">This program will download from the developer's website.</small>
                </div>
                <div class="program__content col-lg-6 col-sm-7">
                    <div class="program__slider">
                        @if( !empty($program->screenshots) )
                            @foreach( $program->screenshots as $screenshot )
                                <div class="program__slider-slide"><img src="{{ $screenshot }}" alt="screenshot"></div>
                            @endforeach
                        @else
                            <div class="program__slider-slide"><img src="/images/noscreen.png" alt="screenshot"></div>
                        @endif
                    </div>
                    <div class="program__description">
                        <ul class="program__description-tabs">
                            <li class="program__description-tabs-item">
                                <a href="#" data-tab="overview" class="program__description-tabs-link program__description-tabs-link_active">Overview</a>
                            </li>
                            <li class="program__description-tabs-item">
                                <a href="#" data-tab="faq" class="program__description-tabs-link">FAQ</a>
                            </li>
                            <li class="program__description-tabs-item">
                                <a href="#" data-tab="uninstall" class="program__description-tabs-link">Uninstall Instruction</a>
                            </li>
                        </ul>
                        <div class="program__description-tabs-content">
                            <div data-tab="overview" class="program__description-tabs-text program__description-tabs-text_active article">
                                <div class="article__heading">{{ $program->name }} Review</div>
                                @if( !empty($program->meta_desc) )
                                    {!! $program->meta_desc !!}
                                @endif
                                @if( !empty($program->desc) )
                                    {!! $program->desc !!}
                                @endif
                                <div class="clearfix"></div>
                                <div class="article__heading">Disclaimer</div>
                                <p class="article__paragraph">Avast Free Antivirus is a product developed by AVAST Software. This site is not directly affiliated with AVAST Software. All trademarks, registered trademarks, product names and company names or logos mentioned herein are the property of their respective owners.</p>
                                <p class="article__paragraph">All programs not hosted on our site. When visitor click "Download" button files will downloading directly from official sources(owners sites).</p>
                            </div>
                            <div data-tab="faq" class="program__description-tabs-text">
                                <div class="article__heading">Frequently Asked Questions</div>
                                <ul class="article__faq">
                                    <li>Why should I download the latest version of {{ $program->name }} for Windows 10?</li>
                                    <p class="article__paragraph">We recommend to download the latest version of {{ $program->name }} because it has the most recent updates, which improves the quality of program.</p>
                                    <li>What's the difference between 64 bit and 32 bit version of {{ $program->name }}?</li>
                                    <p class="article__paragraph">The {{ $program->name }} 64 bit version was specifically designed for 64 bit Windows Operating Systems and performs much better on those.</p>
                                    <li>Will this {{ $program->name }} download work on Windows 10?</li>
                                    <p class="article__paragraph">Yes! The free {{ $program->name }} download for PC works on Windows 10 64 and 32 bits operating systems.</p>
                                </ul>
                            </div>
                            <div data-tab="uninstall" class="program__description-tabs-text">
                                <div class="article__heading">How to uninstall {{ $program->name }}?</div>
                                <p class="article__paragraph">How do I uninstall {{ $program->name }} in Windows 10 / Windows 7 / Windows 8?</p>

                                <ul class="article__list article__list_unstyled">
                                    <li>Click "Start"</li>
                                    <li>Click on "Control Panel"</li>
                                    <li>Under Programs click the Uninstall a Program link.</li>
                                    <li>Select "{{ $program->name }}" and right click, then select Uninstall/Change.</li>
                                    <li>Click "Yes" to confirm the uninstallation.</li>
                                </ul>

                                <p class="article__paragraph">How do I uninstall {{ $program->name }} in Windows 10?</p>
                                <ul class="article__list article__list_unstyled">
                                    <li>Click "Start"</li>
                                    <li>Click on "Control Panel"</li>
                                    <li>Click the Add or Remove Programs icon.</li>
                                    <li>Click on "{{ $program->name }}", then click "Remove/Uninstall."</li>
                                    <li>Click "Yes" to confirm the uninstallation.</li>
                                </ul>

                                <p class="article__paragraph">How do I uninstall {{ $program->name }} in Windows 95, 98, Me, NT, 2000?</p>
                                <ul class="article__list article__list_unstyled">
                                    <li>Click "Start"</li>
                                    <li>Click on "Control Panel"</li>
                                    <li>Double-click the "Add/Remove Programs" icon.</li>
                                    <li>Select "{{ $program->name }}" and right click, then select Uninstall/Change.</li>
                                    <li>Click "Yes" to confirm the uninstallation.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="program__right col-lg-3 col-sm-0">
                    <h2 class="program__right-heading">Software for Windows 10</h2>
                    <div class="program__related">
                        @foreach($random_programs as $item)
                        <a href="{{ Route('program.view', ['slug' => $item->slug]) }}" class="program__related-item">
                            <img src="{{ $item->thumbnail() }}" class="program__related-item-icon">
                            <span class="program__related-item-content">
                                <span class="program__related-item-name">{{ $item->name }}</span>
                                <span class="program__related-item-category">{{ $item->category->name }}</span>
                            </span>
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
