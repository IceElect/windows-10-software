@extends('layouts.info-page')

@section('pageTitle', 'Terms Of Use')

@section('title')
Terms Of Use
@endsection

@section('text')
<p>{{ env('APP_NAME') }} (&ldquo;We&rdquo; or "{{ env('APP_NAME') }}") welcomes you to {{ env('APP_NAME') }} (the &ldquo;Site&rdquo;), a website that enables people to discover new software.</p>
<p>By using or visiting the Site, or using our services, software or products (collectively the &ldquo;Services&rdquo;), you acknowledge that you accept the terms, conditions, restrictions and policies outlined in the Terms of Use below (the &ldquo;Terms&rdquo;). The Services may also provide an opportunity for you to download software or other products, or link to websites (&ldquo;Third Party Services&rdquo;) offered or made available by third parties not affiliated with {{ env('APP_NAME') }} (&ldquo;Third Party Providers&rdquo;). These Terms do not apply to (and We are not responsible for) such Third Party Services, and these Terms do not govern the practices of such Third Party Providers. When utilizing Third Party Services, you should read the relevant terms of use regarding use of such third party websites, software or products, including any applicable end user license agreement (&ldquo;EULA&rdquo;).</p>
<p>&nbsp;</p>
<p>1. Acceptance</p>
<p>1.1 In order to use the Services, you must first agree to the Terms. You may not use the Services if you do not accept the Terms</p>
<p>1.2 You accept these Terms by actually using the Services. You understand and agree that {{ env('APP_NAME') }} will treat your use of the Services as acceptance of the Terms from that point onwards.</p>
<p>1.3 You may not use the Services, and may not accept the Terms, if you are under the age of thirteen (13), or you are a person barred from using or receiving the Services under the laws of the United States or any other relevant jurisdiction, including the country you are resident or from which you use or access the Services.</p>
<p>&nbsp;</p>
<p>2. Use of the Services</p>
<p>2.1 You agree to use the Services only for the purposes that are permitted by these Terms and by any applicable law, regulation, or generally accepted practices or guidelines in any relevant jurisdiction (including any laws governing the export or import of data or software to and from the United States or other relevant jurisdictions).</p>
<p>2.2 You agree that you will not:</p>
<p>(a) engage in any activity that interferes with or disrupts the Services, or the services or networks which are connected to the Services or on which the Services are hosted;</p>
<p>(b) harm {{ env('APP_NAME') }} or its users in any way; or</p>
<p>(c) use the Site in any manner that could damage, disable, overburden, or impair the Site.</p>
<p>2.3 Unless as expressly permitted by {{ env('APP_NAME') }} or a Third Party Provider as defined above, you agree you will not reproduce, duplicate, copy, sell, trade or resell the Services for any purposes.</p>
<p>2.4 You agree that you are solely responsible for, and {{ env('APP_NAME') }} has no responsibility to you or any third party for, any breach of your obligations under the Terms (including breach of any obligations you may have with respect to a Third Party Service) and for the consequences (including any loss or damage which {{ env('APP_NAME') }} may suffer) of any such breach.</p>
<p>&nbsp;</p>
<p>3. Changes to the Services; Updates</p>
<p>3.1 You acknowledge and agree that the form and nature of the Services which {{ env('APP_NAME') }} provides may change from time to time without prior notice to you.</p>
<p>3.2 You acknowledge and agree that {{ env('APP_NAME') }} may stop (whether permanently or temporarily) providing the Services (or any features or software within the Services, including any Third Party Services) to you or to users generally at {{ env('APP_NAME') }}&rsquo;s sole discretion, without prior notice to you.</p>
<p>3.3 You may stop using the Services at any time. Except as otherwise set forth in a separate agreement between {{ env('APP_NAME') }} or with a Third Party Provider, you do not need to provide notice to {{ env('APP_NAME') }} when you stop using the Services.</p>
<p>3.4 Software that you may use, download or install from the Services may automatically download and install updates from time to time from {{ env('APP_NAME') }}. These updates are designed to improve, enhance and further develop the Services and may take the form of bug fixes, enhanced functions, new software modules and completely new versions. You agree to receive such updates (and permit {{ env('APP_NAME') }} to deliver these to you) as part of your use of the Services.</p>
<p>&nbsp;</p>
<p>4. Privacy</p>
<p>4.1 You hereby agree to {{ env('APP_NAME') }}&rsquo;s Privacy Policy, made available online at {{ Route('privacy') }}, as that policy may be changed from time to time.</p>
<p>4.2 If there are material changes to such policy a notice will be posted at {{ Route('privacy') }}, and when any change is made in such policy, the updated policy will be posted at the link above or a successor link. {{ env('APP_NAME') }} recommends that you periodically check for the most current version of the policy.</p>
<p>&nbsp;</p>
<p>5. Third Party Software and Services</p>
<p>5.1 The Services may allow you to download, install, access or use software, products or services provided by Third Party Providers (&ldquo;Third Party Services&rdquo;). Your access or use of the Third Party Services may be subject to applicable third party rights, terms and conditions with respect to such Third Party Services, and may require that you accept additional terms of use for such access or use, including acceptance of a separate EULA.</p>
<p>5.2 You acknowledge and agree that {{ env('APP_NAME') }} is not responsible for any content, websites, software, products or services with respect to the Third Party Services.</p>
<p>5.3 You acknowledge and agree that {{ env('APP_NAME') }} does not warrant or endorse, and does not assume and will not have liability to you or any other person for, any Third Party Services, and that your use of such Third Party Services is at your own risk.</p>
<p>5.4 You acknowledge and agree that your access or use of the Third Party Services may cause information to be shared among your system and such Third Party Service in a manner not governed by these Terms or {{ env('APP_NAME') }}&rsquo;s privacy policies, and that your rights with respect to privacy in such circumstances will be governed solely by the terms of use or privacy policies, if any, for such Third Party Service.</p>
<p>&nbsp;</p>
<p>6. Ownership</p>
<p>6.1 You acknowledge and agree that {{ env('APP_NAME') }}, {{ env('APP_NAME') }}&rsquo;s licensors, or Third Party Providers, own all legal right, title and interest in and to the Services, including any Third Party Services provided on or through the Services, and including any intellectual property rights which subsist in the Services or the Third Party Services (whether those rights happen to be registered or not, and wherever in the world those rights may exist).</p>
<p>6.2 Nothing in the Terms gives you a right to use any {{ env('APP_NAME') }} trade names, trademarks, service marks, logos, domain names or other distinctive brand features.</p>
<p>6.3 You agree that you shall not remove, obscure, or alter any proprietary rights notices (including copyright and trademark notices) which may be affixed or contained within or on the Services.</p>
<p>6.4 Any trade names, trademarks, service marks, logos, domain names or other distinctive brand features used with, on or relating to any Third Party Services (&ldquo;Third Party Marks&rdquo;) available on or through the Services are the property of the Third Party Providers or their respective licensors. Unless you have been expressly authorized in writing by such Third Party Providers or their respective licensors, you will not use, and have no rights in, any Third Party Marks. You further agree that you will not use any trade name, trademark, service mark, logo, domain name, or other distinctive brand feature of any company, entity or organization in any way that is likely or intended to cause confusion as to the owner or authorized user of such names, marks, logos or features.</p>
<p>&nbsp;</p>
<p>7. License from {{ env('APP_NAME') }}</p>
<p>7.1 Except with respect to Third Party Services, the rights of which shall be governed by any terms of use you may enter into with Third Party Providers of such third party products, software or services, {{ env('APP_NAME') }} grants you a limited, personal, non-assignable and non-exclusive license to use the Services provided by {{ env('APP_NAME') }} to you.</p>
<p>7.2 You may not (and may not permit anyone else to) copy, modify, create a derivative work of, reverse engineer, decompile or otherwise attempt to extract the source code of the Services, or any part thereof, unless expressly permitted by {{ env('APP_NAME') }} in writing or as required by law.</p>
<p>7.3 You may not assign (or grant a sublicense to) your rights to use the Services, or otherwise transfer any part of your rights to use the Services.</p>
<p>&nbsp;</p>
<p>8. Term; Termination</p>
<p>8.1 The Terms will continue to apply until terminated by either you or {{ env('APP_NAME') }} as set out below.</p>
<p>8.2 You may terminate these Terms at any time, provided that you cease all use of the Services upon such termination. Upon termination you shall destroy or remove all copies of any {{ env('APP_NAME') }} software provided to you pursuant to the Services, provided, however that any further use of Third Party Services, including software provided pursuant to such Third Party Services, shall continue to be governed by the terms of use or EULA that you may have entered into with the respective Third Party Provider of such software or service.</p>
<p>8.3 Your rights under this Agreement will terminate automatically without notice to you if you violate any provision of these Terms. {{ env('APP_NAME') }} may immediately terminate these Terms and any use of, or access to, the Services at any time, including in the event {{ env('APP_NAME') }} determines that the Services or use thereof may result in infringement or violation of third party rights or claims thereof. Any termination of this Agreement shall also terminate the rights and licenses granted to you hereunder.</p>
<p>8.4 Upon termination, all of the legal rights, obligations and liabilities that you and {{ env('APP_NAME') }} have benefited from, been subject to (or which have accrued over time during the time in which the Terms have been in force) or which are expressed to continue indefinitely, shall be unaffected by such termination, and the provisions of paragraphs 9, 10 and 13.6 shall continue to apply indefinitely.</p>
<p>&nbsp;</p>
<p>9. WARRANTY DISCLAIMER</p>
<p>9.1 NOTHING IN THESE TERMS SHALL EXCLUDE OR LIMIT {{ env('APP_NAME') }}&rsquo;s WARRANTY OR LIABILITY FOR LOSSES WHICH MAY NOT BE LAWFULLY EXCLUDED OR LIMITED BY APPLICABLE LAW. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF CERTAIN WARRANTIES OR CONDITIONS OR THE LIMITATION OR EXCLUSION OF LIABILITY FOR LOSS OR DAMAGE CAUSED BY NEGLIGENCE, BREACH OF CONTRACT OR BREACH OF IMPLIED TERMS, OR INCIDENTAL OR CONSEQUENTIAL DAMAGES. ACCORDINGLY, ONLY THE LIMITATIONS WHICH ARE LAWFUL IN YOUR JURISDICTION WILL APPLY TO YOU AND {{ env('APP_NAME') }}&rsquo;s LIABILITY WILL BE LIMITED TO THE MAXIMUM EXTENT PERMITTED BY LAW.</p>
<p>9.2 YOU EXPRESSLY UNDERSTAND AND AGREE THAT YOUR USE OF THE SERVICES IS AT YOUR SOLE RISK AND THAT THE SERVICES ARE PROVIDED &ldquo;AS IS&rdquo; AND &ldquo;AS AVAILABLE.&rdquo;</p>
<p>9.3 IN PARTICULAR, {{ env('APP_NAME') }}, ITS SUBSIDIARIES AND AFFILIATES, AND ITS LICENSORS DO NOT REPRESENT OR WARRANT TO YOU THAT:</p>
<p>(a) YOUR USE OF THE SERVICES WILL MEET YOUR REQUIREMENTS;</p>
<p>(b) YOUR USE OF THE SERVICES WILL BE UNINTERRUPTED, TIMELY, SECURE OR FREE FROM ERROR;</p>
<p>(c) ANY INFORMATION OBTAINED BY YOU AS A RESULT OF YOUR USE OF THE SERVICES WILL BE ACCURATE OR RELIABLE; AND</p>
<p>(d) THAT DEFECTS IN THE OPERATION OR FUNCTIONALITY OF ANY SOFTWARE PROVIDED TO YOU AS PART OF THE SERVICES WILL BE CORRECTED.</p>
<p>9.4 ANY SOFTWARE OR OTHER MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE USE OF THE SERVICES IS DONE AT YOUR OWN DISCRETION AND RISK AND THAT YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR OTHER DEVICE OR LOSS OF DATA THAT RESULTS FROM THE DOWNLOAD OF ANY SUCH SOFTWARE OR OTHER MATERIAL.</p>
<p>9.5 NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM {{ env('APP_NAME') }} OR THROUGH OR FROM THE SERVICES SHALL CREATE ANY WARRANTY NOT EXPRESSLY STATED IN THE TERMS.</p>
<p>9.6 {{ env('APP_NAME') }} FURTHER EXPRESSLY DISCLAIMS ALL WARRANTIES AND CONDITIONS OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO THE IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.</p>
<p>&nbsp;</p>
<p>10. LIMITATION OF LIABILITY</p>
<p>10.1 SUBJECT TO PARAGRAPH 9.1 ABOVE, YOU EXPRESSLY UNDERSTAND AND AGREE THAT {{ env('APP_NAME') }}, ITS SUBSIDIARIES AND AFFILIATES, AND ITS LICENSORS SHALL NOT BE LIABLE TO YOU FOR:</p>
<p>(a) ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES WHICH MAY BE INCURRED BY YOU, HOWEVER CAUSED AND UNDER ANY THEORY OF LIABILITY. THIS SHALL INCLUDE, BUT NOT BE LIMITED TO, ANY LOSS OF PROFIT (WHETHER INCURRED DIRECTLY OR INDIRECTLY), ANY LOSS OF GOODWILL OR BUSINESS REPUTATION, ANY LOSS OF DATA SUFFERED, COST OF PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES, OR OTHER INTANGIBLE LOSS; OR</p>
<p>(b) ANY LOSS OR DAMAGE WHICH MAY BE INCURRED BY YOU, INCLUDING BUT NOT LIMITED TO, LOSS OR DAMAGE AS A RESULT OF ANY CHANGES WHICH {{ env('APP_NAME') }} MAY MAKE TO THE SERVICES, OR FOR ANY PERMANENT OR TEMPORARY CESSATION IN THE PROVISION OF THE SERVICES (OR ANY FEATURES WITHIN THE SERVICES).</p>
<p>10.2 THE LIMITATIONS ON {{ env('APP_NAME') }}&rsquo;s LIABILITY TO YOU IN PARAGRAPH 10.1 ABOVE SHALL APPLY WHETHER OR NOT {{ env('APP_NAME') }} HAS BEEN ADVISED OF OR SHOULD HAVE BEEN AWARE OF THE POSSIBILITY OF ANY SUCH LOSSES ARISING.</p>
<p>&nbsp;</p>
<p>11. DMCA Notifications</p>
<p>11.1 It is {{ env('APP_NAME') }}&rsquo;s policy to respond to notices of alleged copyright infringement that comply with applicable international intellectual property law (including in the United States, the Digital Millennium Copyright Act) and to terminate the accounts of repeat infringers. Notifications regarding any such alleged infringement should be emailed to copyright agent {{ env('APP_NAME') }}</p>
<p>&nbsp;</p>
<p>12. Changes to the Terms</p>
<p>12.1 {{ env('APP_NAME') }} may make changes to these Terms or provide revised or additional terms from time to time. When these changes are made, {{ env('APP_NAME') }} will make an updated version of these Terms available at http://www.{{ env('APP_NAME') }}/info/term-of-use.html. You understand and agree that if you continue to use the Services after the date upon which the Terms have been revised or changed, {{ env('APP_NAME') }} will treat your use as acceptance of the updated Terms.</p>
<p>&nbsp;</p>
<p>13. Other Miscellaneous Terms</p>
<p>13.1 The Terms constitute the whole legal agreement between you and {{ env('APP_NAME') }} and govern your use of the Services (but excluding any services which {{ env('APP_NAME') }} may provide to you under a separate written agreement), and completely replace any prior agreements between you and {{ env('APP_NAME') }} in relation to the Services.</p>
<p>13.2 You agree that {{ env('APP_NAME') }} may provide you with notices, including those regarding changes to the Terms, by email, regular mail, or postings on the Services.</p>
<p>13.3 You agree that if {{ env('APP_NAME') }} does not exercise or enforce any legal right or remedy which is contained in the Terms (or which {{ env('APP_NAME') }} has the benefit of under any applicable law), this will not be taken to be a formal waiver of {{ env('APP_NAME') }}&rsquo;s rights and that those rights or remedies will still be available to {{ env('APP_NAME') }}.</p>
<p>13.4 If any court of law, having the jurisdiction to decide on this matter, rules that any provision of these Terms is invalid, then that provision will be removed from the Terms without affecting the rest of the Terms. The remaining provisions of the Terms will continue to be valid and enforceable.</p>
<p>13.5 You acknowledge and agree that each member of the group of companies of which {{ env('APP_NAME') }} is the parent shall be third party beneficiaries to the Terms and that such other companies shall be entitled to directly enforce, and rely upon, any provision of the Terms which confers a benefit on (or rights in favor of) them. Other than this, no other person or company shall be third party beneficiaries to the Terms.</p>
<p>13.6 The Terms, and your relationship with {{ env('APP_NAME') }} under the Terms, shall be governed by the laws of the State of California without regard to its conflict of laws provisions. You and {{ env('APP_NAME') }} agree to submit to the exclusive jurisdiction of the courts located within the county of San Diego, California to resolve any legal matter arising from the Terms. Notwithstanding this, you agree that {{ env('APP_NAME') }} shall still be allowed to apply for injunctive remedies (or an equivalent type of urgent legal relief) in any jurisdiction.</p>
@endsection
