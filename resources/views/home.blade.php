@extends('layouts.app')

@section('pageTitle', 'Download Latest versions of Software')

@section('head')
    <meta name="description" content="Need fresh software for Windows 10 64 or 32 bit? Download Latest versions on DownloadLab!"/>
    <meta name="keywords" content="software for windows 10,programs for windows 10,download for windows 10,Windows 10 Software"/>
@endsection

@section('content')
    <section>
        <div class="container">
            <div class="program-list">
                <div class="program-list__inner">
                    @foreach($programs as $item)
                        <a href="{{ Route('program.view', ['slug' => $item->slug]) }}" class="program-list__item">
                            {{-- <img class="program-list__item-thumb" src="{{ getProgramThumbnail( $item->slug ) }}" alt="{{ $item->name }}"> --}}
                            <img class="program-list__item-thumb" src="{{ $item->thumbnail() }}" alt="{{ $item->name }}">
                            <div class="program-list__item-name">{{ $item->name }}</div>
                            <div class="program-list__item-category">{{ $item->category->name }}</div>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection
