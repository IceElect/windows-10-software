<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ProgramController;
use App\Http\Controllers\ServiceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ProgramController::class, 'index'])->name('home');

Route::get('/eula', function() {
    return view('eula');
})->name('eula');

Route::get('/terms', function() {
    return view('terms');
})->name('terms');

Route::get('/privacy', function() {
    return view('privacy');
})->name('privacy');

Route::get('/contacts', function() {
    return view('contacts');
})->name('contacts');

Route::get('/service', [ServiceController::class, 'index'])->name('service');
Route::get('/{slug}', [ProgramController::class, 'view'])->name('program.view');
Route::get('/{slug}/', [ProgramController::class, 'view'])->name('program.view');
Route::get('/{slug}/download', [ProgramController::class, 'download'])->name('program.download');
