<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Program;

class ProgramController extends Controller
{
    public function index(Request $request) {
        $q = $request->input('q');

        if($q) {
            $searchTerm = "%{$q}%";
            $programs = Program::where('name', 'like', $searchTerm)->get();
        } else {
            $programs = Program::all();
        }

        return view('home')->with([
            'q' => $q,
            'programs' => $programs
        ]);
    }

    public function view($slug) {
        $program = Program::where(['slug' => $slug])->first();

        if(empty($program)) {
            abort(404);
        }

        $program->screenshots = $program->screenshots();
        $program->installer = $program->installer();
        $program->downloads_count = $program->downloads_count(
            $program->installer['size']
        );

        return view('program.view')->with([
            'program' => $program,
            'isOS' => ($program->category == 'Operating System'),
            'random_programs' => Program::inRandomOrder()->limit(5)->get()
        ]);
    }

    public function download($slug) {
        $program = Program::where(['slug' => $slug])->first();

        if(empty($program)) {
            abort(404);
        }

        $program->installer = $program->installer();

        return view('program.download')->with([
            'program' => $program,
            'random_programs' => Program::inRandomOrder()->limit(6)->get()
        ]);
    }
}
