<?php

if (! function_exists('formatSize')) {
    function formatSize($size){
         $metrics[0] = 'bytes';
         $metrics[1] = 'KB';
         $metrics[2] = 'MB';
         $metrics[3] = 'GB';
         $metrics[4] = 'TB';
         $metric = 0;
         while(floor($size/1024) > 0){
             ++$metric;
             $size /= 1024;
         }
         $ret =  round($size,2)." ".(isset($metrics[$metric])?$metrics[$metric]:'??');
         return $ret;
    }
}

if (! function_exists('isSearchBot')) {
    function isSearchBot() {
        if (!empty($_SERVER['HTTP_USER_AGENT'])) {
    		$options = array(
    			'YandexBot', 'YandexAccessibilityBot', 'YandexMobileBot','YandexDirectDyn',
    			'YandexScreenshotBot', 'YandexImages', 'YandexVideo', 'YandexVideoParser',
    			'YandexMedia', 'YandexBlogs', 'YandexFavicons', 'YandexWebmaster',
    			'YandexPagechecker', 'YandexImageResizer','YandexAdNet', 'YandexDirect',
    			'YaDirectFetcher', 'YandexCalendar', 'YandexSitelinks', 'YandexMetrika',
    			'YandexNews', 'YandexNewslinks', 'YandexCatalog', 'YandexAntivirus',
    			'YandexMarket', 'YandexVertis', 'YandexForDomain', 'YandexSpravBot',
    			'YandexSearchShop', 'YandexMedianaBot', 'YandexOntoDB', 'YandexOntoDBAPI',
    			'Googlebot', 'Googlebot-Image', 'Mediapartners-Google', 'AdsBot-Google',
    			'Mail.RU_Bot', 'bingbot', 'Accoona', 'ia_archiver', 'Ask Jeeves',
    			'OmniExplorer_Bot', 'W3C_Validator', 'WebAlta', 'YahooFeedSeeker', 'Yahoo!',
    			'Ezooms', '', 'Tourlentabot', 'MJ12bot', 'AhrefsBot', 'SearchBot', 'SiteStatus',
    			'Nigma.ru', 'Baiduspider', 'Statsbot', 'SISTRIX', 'AcoonBot', 'findlinks',
    			'proximic', 'OpenindexSpider','statdom.ru', 'Exabot', 'Spider', 'SeznamBot',
    			'oBot', 'C-T bot', 'Updownerbot', 'Snoopy', 'heritrix', 'Yeti',
    			'DomainVader', 'DCPbot', 'PaperLiBot'
    		);

    		foreach($options as $row) {
    			if (stripos($_SERVER['HTTP_USER_AGENT'], $row) !== false) {
    				return true;
    			}
    		}
    	}

    	return false;
    }
}
