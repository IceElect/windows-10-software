<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Program extends Model
{
    function category() {
        return $this->belongsTo('App\ProgramCategory', 'category_id');
    }

    function thumbnail() {
        $path = "download-{$this->slug}/thumbnail/thumbnail.png";
        $exists = Storage::disk('public')->exists($path);
        if($exists) {
            return Storage::disk('public')->url($path);
        }
    }

    function screenshots() {
        $files = [];
        $path = "download-{$this->slug}/screenshot";
        $aFiles = Storage::disk('public')->files($path);
        foreach($aFiles as $file) {
            $files[] = Storage::disk('public')->url($file);
        }
        return $files;
    }

    function installer() {
        $data = [
            'path' => $this->site,
            'size' => ($this->file_size) ? $this->file_size : '5.23 MB'
        ];
        $path = "download-{$this->slug}/installer";
        $aFiles = Storage::disk('public')->files($path);
        $path = (count($aFiles)) ? $aFiles[0] : false;

        if($path) {
            if(isSearchBot())
                $data['path'] = Storage::disk('public')->url($path);

            $data['size'] = formatSize( Storage::disk('public')->size($path) );
        }

        return $data;
    }

    function downloads_count($file_size = 1) {
        return substr(round((strlen($this->name) * strlen($this->company) * (float) $file_size) * 256,0),0,3).' '.substr(round((strlen($this->name) * strlen($this->company) * (float) $file_size) * 512,0),2,3);
    }
}
